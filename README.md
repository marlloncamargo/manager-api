# Manager API

## Start Application


```bash
mvn clean install
mvn spring-boot:run
```

## Swagger

```
http://localhost:8080/swagger-ui.html
```