package com.manager.api.managerapi.repository;

import com.manager.api.managerapi.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
  Optional<Customer> findByAddresses_ZipCode(String zipCode);
}
