package com.manager.api.managerapi.repository;

import com.manager.api.managerapi.model.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface AddressRepository extends JpaRepository<Address, Long> {
  @Query("select ad from Address ad join ad.customers ca where ca.documentId = :customer_id")
  Set<Address> findByCustomers(@Param("customer_id") Long documentId);
}
