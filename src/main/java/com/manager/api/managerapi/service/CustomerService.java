package com.manager.api.managerapi.service;

import com.manager.api.managerapi.model.Customer;

import java.util.List;
import java.util.Optional;

public interface CustomerService {
  List<Customer> findAll();

  Optional<Customer> findById(Long customerId);

  Optional<Customer> findByZipCode(String zipCode);

  Customer saveCustomer(Customer customer);

  Optional<Customer> updateCustomer(Long customerId, Customer customer);

  void deleteCustomer(Long customerId);
}
