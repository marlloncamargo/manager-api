package com.manager.api.managerapi.service.impl;

import com.manager.api.managerapi.model.Customer;
import com.manager.api.managerapi.repository.AddressRepository;
import com.manager.api.managerapi.repository.CustomerRepository;
import com.manager.api.managerapi.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CustomerServiceImpl implements CustomerService {

  @Autowired private CustomerRepository repository;
  @Autowired private AddressRepository addressRepository;


  @Override
  public List<Customer> findAll() {
    return repository.findAll();
  }

  @Override
  public Optional<Customer> findById(Long customerId) {
    return repository.findById(customerId);
  }

  @Override
  public Optional<Customer> findByZipCode(String zipCode) {
    return repository.findByAddresses_ZipCode(zipCode);
  }

  public Customer saveCustomer(Customer customer) {
    if (!customer.getAddresses().isEmpty()) {
      customer.getAddresses()
              .forEach(ad -> addressRepository.save(ad));
    }

    return repository.save(customer);
  }

  public Optional<Customer> updateCustomer(Long customerId, Customer customer){
    return repository.findById(customerId)
            .map(record -> {
              record.setDocumentId(customer.getDocumentId());
              record.setName(customer.getName());
              record.setAge(customer.getAge());
              Customer updated = repository.save(record);
              return updated;
            });
  }

  @Override
  public void deleteCustomer(Long customerId) {
    repository.findById(customerId)
            .map(record -> {
              repository.deleteById(customerId);
              return ResponseEntity.ok().build();
            }).orElse(ResponseEntity.notFound().build());
  }
}
