package com.manager.api.managerapi.controller;

import com.manager.api.managerapi.model.Customer;
import com.manager.api.managerapi.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/customer")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CustomerController {

  @Autowired
  private CustomerService service;

  @GetMapping
  public List<Customer> findAll() {
    return service.findAll();
  }

  @GetMapping(path = "/{id}")
  public ResponseEntity<Customer> findById(@PathVariable Long id) {
    return service.findById(id)
            .map(record -> ResponseEntity.ok().body(record))
            .orElse(ResponseEntity.notFound().build());
  }

  @GetMapping(path = "/address/zipcode/{zipcode}")
  public ResponseEntity<Customer> findByZipCode(@PathVariable String zipcode) {
    return service.findByZipCode(zipcode)
            .map(record -> ResponseEntity.ok().body(record))
            .orElse(ResponseEntity.notFound().build());
  }

  @PostMapping
  public Customer saveCustomer(@RequestBody Customer customer) {
    return service.saveCustomer(customer);
  }

  @PutMapping(value = "/{id}")
  public Optional<Customer> updateCustomer(@PathVariable("id") Long customerId, @RequestBody Customer customer){
    return service.updateCustomer(customerId, customer);
  }

  @DeleteMapping(path = "/{id}")
  public void deleteCustomer(@PathVariable Long id){
    service.deleteCustomer(id);
  }

}
