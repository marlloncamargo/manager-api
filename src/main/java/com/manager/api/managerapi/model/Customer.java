package com.manager.api.managerapi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Table(name = "customer")
public class Customer {

  @Id
  private Long documentId;
  private String name;
  private Integer age;

  @JsonIgnore
  @CreationTimestamp
  private LocalDateTime registrationDate;

  @JsonIgnore
  @UpdateTimestamp
  private LocalDateTime lastUpdate;

  @ManyToMany(cascade = CascadeType.ALL)
  @JoinTable(name = "customer_address",
          joinColumns = @JoinColumn(name = "customer_id", referencedColumnName = "documentId"),
          inverseJoinColumns = @JoinColumn(name = "address_id", referencedColumnName = "id"))
  private Set<Address> addresses;

  public Long getDocumentId() {
    return documentId;
  }

  public void setDocumentId(Long documentId) {
    this.documentId = documentId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getAge() {
    return age;
  }

  public void setAge(Integer age) {
    this.age = age;
  }

  public LocalDateTime getRegistrationDate() {
    return registrationDate;
  }

  public void setRegistrationDate(LocalDateTime registrationDate) {
    this.registrationDate = registrationDate;
  }

  public LocalDateTime getLastUpdate() {
    return lastUpdate;
  }

  public void setLastUpdate(LocalDateTime lastUpdate) {
    this.lastUpdate = lastUpdate;
  }

  public Set<Address> getAddresses() {
    return addresses;
  }

  public void setAddresses(Set<Address> addresses) {
    this.addresses = addresses;
  }
}
